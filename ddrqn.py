'''

Batch Constrained Deep Q-learning and Double DQN algorithms

Code related to the papers: Benchmarking Batch Deep Reinforcement Learning Algorithms &
Deep Reinforcement Learning with Double Q-learning

Source: https://gitlab.com/hdonancio/pumpscheduling

'''

import random
import functools
import numpy as np
import tensorflow as tf
import prioritized_replay # Module to prepare data for PER
import bcq # Module to use BCQ
from collections import deque
from keras.optimizers import Adam
from keras.models import Sequential
from keras.layers import Dense, LSTM, Activation
from keras import regularizers

from tensorflow.compat.v1 import ConfigProto
from tensorflow.compat.v1 import InteractiveSession
config = ConfigProto()
config.gpu_options.allow_growth = True
session = InteractiveSession(config=config)

# Model path
MODEL_PATH = './MODEL/WDS_Q0'
# The number of times that data will be generated (number of replay memory per dataset)
TRAINING_SIZE = 1
# STATE space dimension = (tank1_level_t, water_consumption , time of day (t), month, last_action, time_running, water quality)
# where tank level and demand of water at time t (in the format of ms) are continuous values; month and
# last action are one-hot encode, time_running discrete, and water quality binary.
STATE_SPACE = 25
# ACTION space dimension = (nop, np1, np2, np3, np4) where all values are binary indicating if the pump is active or not
ACTION_SPACE = 5
# Actions available
ACTIONS = [[0,0,0,0], [1,0,0,0], [0,1,0,0], [0,0,1,0], [0,0,0,1]]
# How many (max) transitions to keep for model training
MAX_SIZE_MEMORY = 1051200
# Discount factor  
DISCOUNT_FACTOR = 0.99
# Num of hidden nodes in hidden layer
HIDDEN_NODES = 100
# Size of expanded state
STATE_SIZE = 4
# Minibatch size
MINIBATCH_SIZE = 36
# How often update the target network
UPDATE_TARGET = 12000
# Learning rate of the NN
LEARNING_RATE = 3e-5
# Epochs or scale of training for PER
EPOCHS = 200/TRAINING_SIZE
# ** Parameters related to Prioritized Experience Replay **
PER = True
# Error upper bound
ABS_ERROR_UPPER = 1
# Batch Contrained Q-learning
BCQ = True

class DDRQN:
	def create_model(self):

		model = Sequential()  
		# Input shape of LSTM (layer 0) is (batch_size, time_steps, num_of_features)
		model.add(LSTM(HIDDEN_NODES, input_shape=(STATE_SIZE, STATE_SPACE), return_sequences=False))
		model.add(Dense(HIDDEN_NODES, activation='relu', kernel_regularizer=regularizers.l2(0.000001)))
		model.add(Dense(HIDDEN_NODES, activation='relu', kernel_regularizer=regularizers.l2(0.000001)))
		model.add(Dense(ACTION_SPACE, activation='linear'))

		# Compile the model
		model.compile(loss='mse', optimizer=Adam(learning_rate=LEARNING_RATE, clipnorm=1))

		return model

	def get_model(self):
		# Load the trained model
		self.model = tf.keras.models.load_model(MODEL_PATH)
		# Copy the weights for the target model
		self.target_model = self.create_model()
		self.target_model.set_weights(self.model.get_weights())

	def feed_memory(self):
		# Open the .txt file with the transitions and store it in the replay memory
		with open ("replay_memory.txt", "r") as file:
			for line in file:
				line.rstrip()
				if line.find('\x00') == -1:
					transition = eval(line)
					self.replay_memory.append(transition)
		self.replay_memory_size = len(self.replay_memory)		

	def train_model(self):
		# Count the number of final states to update the target net
		update_counter = 0
		# Control the number of samples in training
		samples_counter = 0
		minibatch_counter = 0
		# Reset the batch
		self.batch = []
		for i in range(STATE_SIZE, self.replay_memory_size + 1):
			expanded_state = []
			expanded_next_state = []
			for j in range(0, STATE_SIZE):
				transition = self.replay_memory[i - STATE_SIZE + j]
				expanded_state += transition[0]
				expanded_next_state += transition[2]
				self.batch.append([expanded_state, transition[1], expanded_next_state, transition[3], transition[4]])

		# Shuffle the batch to break correlation between data
		random.shuffle(self.batch)
		self.batch_size = len(self.batch)

		# If using PER	
		if PER:
			for transition in self.batch:
				# Find the currently transition with max priority
				max_priority = self.per.get_max()

				# Get the max priority for new samples
				if max_priority == 0:
					max_priority = ABS_ERROR_UPPER

				# Add the sample to PER memory	
				self.per.add(transition, max_priority)
				samples_counter += 1	

		elif not PER:		
			# Create mini batches for the training	
			shuffled_batch = list()	
			for j in range(int(self.batch_size/MINIBATCH_SIZE)):
				mini_batch = list()
				for i in range(0, MINIBATCH_SIZE):
					transition = self.batch[i + (j * MINIBATCH_SIZE)]
					mini_batch.append(transition)
				shuffled_batch.append(mini_batch)

			self.batch = shuffled_batch
			shuffled_batch = []

			# Update to count the number of minibatches
			self.batch_size = len(self.batch)

		while True:
			if PER:
				if samples_counter * EPOCHS < MINIBATCH_SIZE:
					break
				else:
					b_idx, minibatch, ISWeights = self.per.sample(MINIBATCH_SIZE)
			else:
				if minibatch_counter >= self.batch_size - 1:
					break
				else:
					minibatch = self.batch[minibatch_counter]
					minibatch_counter += 1

			current_state = np.zeros((MINIBATCH_SIZE, STATE_SIZE, STATE_SPACE))

			next_state = np.zeros((MINIBATCH_SIZE, STATE_SIZE, STATE_SPACE))

			action, reward, terminal = [], [], []
			# Get the transitions in replay memory
			for (i, transition) in enumerate(minibatch):
				# Get the current state
				current_state[i] = np.reshape(transition[0], ((1, STATE_SIZE, STATE_SPACE)))
				# Get action
				action.append(transition[1])
				# Get next states 
				next_state[i] = np.reshape(transition[2], ((1, STATE_SIZE, STATE_SPACE)))
				# Get the reward
				reward.append(transition[3])
				# If is terminal state or not
				terminal.append(transition[4])

			# Query NN model for Q values	
			current_qs_list = self.model.predict(current_state)

			next_qs_list = self.model.predict(next_state)
			
			future_qs_list = self.target_model.predict(next_state)

			qs_target = current_qs_list.copy()

			for i in range(MINIBATCH_SIZE):
				# Check if the state is a terminal state
				if terminal[i]:
					current_qs_list[i][ACTIONS.index(action[i])] = reward[i]
				else:
					print(next_qs_list[i])
					if BCQ:
						max_future_q = self.bcq.get_qmax(next_state[i][STATE_SIZE-1], future_qs_list[i])
					else:
						max_future_q = 	future_qs_list[i][np.argmax(next_qs_list[i])]
					current_qs_list[i][ACTIONS.index(action[i])] = reward[i] + DISCOUNT_FACTOR * max_future_q
					print(current_qs_list[i])
					print('---------------------------------------------------------')
				update_counter += 1
				samples_counter -= 1

			if PER:
				# Calculate the abs error of the gradient	
				abs_errors = tf.reduce_sum(tf.abs(qs_target - current_qs_list), axis=1)
				loss = self.model.fit(current_state, current_qs_list, batch_size=MINIBATCH_SIZE, epochs=1, sample_weight=ISWeights, verbose=0)
				self.per.batch_update(b_idx, abs_errors)
				# self.log.write(str(abs_errors)+'\n')
			# else:	
				# Calculate the loss and record it in the log file
				# loss = self.model.fit(current_state, current_qs_list, batch_size=MINIBATCH_SIZE, epochs=EPOCHS, verbose=0)
				# avg_loss = np.mean(loss.history['loss']) 
				# self.log.write(str(avg_loss)+'\n')

			# Update the target network
			if update_counter > UPDATE_TARGET:
				self.target_model.set_weights(self.model.get_weights())
				update_counter = 0		
		# Save the model		
		self.model.save(MODEL_PATH)

	def __init__(self):

		# Initialize Replay Memory
		self.replay_memory = deque(maxlen=MAX_SIZE_MEMORY)

		# If PER, initialize it
		if PER:
			self.per = prioritized_replay.PER(MAX_SIZE_MEMORY, ABS_ERROR_UPPER)
		# If BCQ, initialize it	
		if BCQ:
			self.bcq = 	bcq.GenerativeModel()

		# Create a new model
		self.model = self.create_model()

		# Target network
		self.target_model = self.create_model()
		self.target_model.set_weights(self.model.get_weights())	

		self.model.save(MODEL_PATH)
		print('--------------------------------------------------------------------------------------------')
		print('#########################  DOUBLE DQN/BATCH CONSTRAINED Q-LEARNING  ########################')
		print('--------------------------------------------------------------------------------------------')
