import math
import datetime
import numpy as np
import pandas as pd
from operator import add
#import tensorflow as tf
import matplotlib.pyplot as plt

# Path where the data is stored
DATA_PATH = './Data/'
# Data that will be loaded to feed the replay memory
YEAR = '2012'
MONTHS = ['Januar', 'Februar', 'Maerz', 'April', 'Mai', 'Juni', 'Juli', 'August', 'September', 'Oktober', 'November', 'Dezember']
MONTHSL = ['January', 'February', 'March', 'April', 'May', 'June', 'July', 'August', 'September', 'October', 'November', 'December']

# One-hot encoding for Months
MONTH_ENCODING = [[1,0,0,0,0,0,0,0,0,0,0,0], 
				  [0,1,0,0,0,0,0,0,0,0,0,0],
				  [0,0,1,0,0,0,0,0,0,0,0,0],
				  [0,0,0,1,0,0,0,0,0,0,0,0], 
				  [0,0,0,0,1,0,0,0,0,0,0,0],
				  [0,0,0,0,0,1,0,0,0,0,0,0],
				  [0,0,0,0,0,0,1,0,0,0,0,0],
				  [0,0,0,0,0,0,0,1,0,0,0,0],
				  [0,0,0,0,0,0,0,0,1,0,0,0],
				  [0,0,0,0,0,0,0,0,0,1,0,0],
				  [0,0,0,0,0,0,0,0,0,0,1,0],
				  [0,0,0,0,0,0,0,0,0,0,0,1]]

# Actions available
ACTIONS = [[0,0,0,0], [1,0,0,0], [0,1,0,0], [0,0,1,0], [0,0,0,1]]

class Evaluation:

	def evaluate(self):	

		# File to store the evaluation results
		f = open("real_data_statistics.txt", "w")

		#---------------------------------------------------------------------------------------------#
		################################# VARIABLES OF THE SIMULATOR ##################################
		#---------------------------------------------------------------------------------------------#

		Q1 = [0.0, 320.0, 570.0, 670.0, 897.0, 1099.0, 1298.0, 1597.3, 1823.2]
		H1 = [76.81, 74.70, 71.61, 71.75, 72.29, 69.99, 67.95, 61.88, 55.79]
		ETA1 = [0.00, 38.60, 55.55, 60.68, 72.28, 78.85, 83.12, 85.96, 85.07]

		Q2 = [0.0, 240.0, 494.0, 694.0, 899.0, 1095.7, 1209.3, 1465.3]
		H2 = [74.15, 72.15, 70.78, 69.34, 66.44, 62.75, 60.12, 52.57]
		ETA2 = [0.00, 40.65, 63.22, 75.36, 83.02, 87.09, 88.32, 87.11]

		Q3 = [0.0, 237.0, 470.0, 645.0, 797.3, 1000.3, 1208.7, 1410.0] 
		H3 = [63.84, 62.46, 61.98, 60.61, 58.68, 54.87, 50.07, 43.83]
		ETA3 = [0.00, 38.28, 61.02, 72.36, 79.05, 84.94, 87.08, 84.91]

		Q4 = [0.0, 195.0, 440.0, 659.7, 806.3, 998.7, 1204.0]
		H4 = [64.13, 63.19, 61.62, 59.35, 56.61, 51.21, 43.71]
		ETA4 = [0.00, 41.95, 67.50, 80.96, 84.84, 86.04, 83.42]

		A = math.pi*math.pow(44,2)/4;
		rho = 1000
		g = 9.81
		p1 = -2.713E-09
		p2 = 0.000006504
		p3 = 4.384E-07
		p4 = -0.0001768

		Qanlage = np.linspace(0,2000,2000)
		p = np.polyfit(Q1,H1,2)
		H1 = np.polyval(p,Qanlage)
		pETA1 = np.polyfit(Q1,ETA1,2)
		ETA1 = np.polyval(pETA1,Qanlage)
		p = np.polyfit(Q2,H2,2)
		H2 = np.polyval(p,Qanlage)
		pETA2 = np.polyfit(Q2,ETA2,2)
		ETA2 = np.polyval(pETA2,Qanlage)
		p = np.polyfit(Q3,H3,2)
		H3 = np.polyval(p,Qanlage)
		pETA3 = np.polyfit(Q3,ETA3,2)
		ETA3 = np.polyval(pETA3,Qanlage)
		p = np.polyfit(Q4,H4,2)
		H4 = np.polyval(p,Qanlage)
		pETA4 = np.polyfit(Q4,ETA4,2)
		ETA4 = np.polyval(pETA4,Qanlage)

		###############################################################################################

		# Store average consume daily by month
		average_electricity_month_np1 = np.zeros(12)
		average_electricity_month_np2 = np.zeros(12)
		average_electricity_month_np3 = np.zeros(12)
		average_electricity_month_np4 = np.zeros(12)
		average_electricity_month = np.zeros(12)

		# Store average cycles daily by month
		average_cycles_month_np1 = np.zeros(12)
		average_cycles_month_np2 = np.zeros(12)
		average_cycles_month_np3 = np.zeros(12)
		average_cycles_month_np4 = np.zeros(12)

		# Store average tank level
		average_tank_level = np.zeros((24,12))

		# Tank level
		HB = []
		# Tank level of the first timestep in real data 2012
		HB.append(53.42)

		last_action = 2

		total_consume = 0

		for indexm, month in enumerate(MONTHS):
			# Number of days in the month
			days_counter = 0

			# Minutes counter
			minutes_counter = 0

			# Consume of electricity
			consume_np1 = 0
			consume_np2 = 0
			consume_np3 = 0
			consume_np4 = 0

			# Number of switches in the settings
			switches_np1 = 0
			switches_np2 = 0
			switches_np3 = 0
			switches_np4 = 0

			# Tank level sum
			tk1_level = 0

			# Open dataframes
			data_time = pd.read_csv(DATA_PATH+YEAR+'/WaterConsumption/'+month+'.csv', delimiter=';', skip_blank_lines = True)[['Time']]
			data_date = pd.read_csv(DATA_PATH+YEAR+'/WaterConsumption/'+month+'.csv', delimiter=';', decimal=',', skip_blank_lines = True)[['Date']]
			data_waterConsumption = pd.read_csv(DATA_PATH+YEAR+'/WaterConsumption/'+month+'.csv', delimiter=';', decimal=',', skip_blank_lines = True)[['Netzverbrauch_pval']]

			data_qr_np1 = pd.read_csv(DATA_PATH+YEAR+'/NP/NP1/Q/'+month+'.csv', delimiter=';', decimal=',', skip_blank_lines = True)[['NP_1_Volumenfluss_pval']]
			data_qr_np2 = pd.read_csv(DATA_PATH+YEAR+'/NP/NP2/Q/'+month+'.csv', delimiter=';', decimal=',', skip_blank_lines = True)[['NP_2_Volumenfluss_pval']]
			data_qr_np3 = pd.read_csv(DATA_PATH+YEAR+'/NP/NP3/Q/'+month+'.csv', delimiter=';', decimal=',', skip_blank_lines = True)[['NP_3_Volumenfluss_pval']]
			data_qr_np4 = pd.read_csv(DATA_PATH+YEAR+'/NP/NP4/Q/'+month+'.csv', delimiter=';', decimal=',', skip_blank_lines = True)[['NP_4_Volumenfluss_pval']]

			# Granularity of data in this csv file is higher than others. Exclude some lines to make it equal in timesteps
			data_time_np1_q = pd.read_csv(DATA_PATH+YEAR+'/NP/NP1/Q/'+month+'.csv', delimiter=';', skip_blank_lines = True)[['Time']]
			for i, d in data_time_np1_q.iterrows():
				h,m,s = data_time_np1_q.iloc[i, 0].split(':')
				if(i == 0):
					h_i,m_i,s_i = data_time_np1_q.iloc[i, 0].split(':')
					# Reset the list of the tank levels
					HB_temp = HB[len(HB) - 1]
					HB = []
					HB.append(HB_temp)
				elif(m_i == m):
					data_qr_np1.drop([i], inplace=True)
				else: 
					m_i = m

			data_qr_np1.reset_index(drop=True, inplace=True)					

			# Fix duplicated data in Water Consumption
			data_time_waterConsumption = pd.read_csv(DATA_PATH+YEAR+'/WaterConsumption/'+month+'.csv', delimiter=';', skip_blank_lines = True)[['Time']]
			for i, d in data_time_waterConsumption.iterrows():
				h,m,s = data_time_waterConsumption.iloc[i, 0].split(':')
				if i == 0:
					h_i,m_i,s_i = data_time_waterConsumption.iloc[i, 0].split(':')
				elif int(m) == 0:
					if h_i == h:
						j = 0
						while h_i == h:
							data_time.drop([i + j], inplace=True)
							data_date.drop([i + j], inplace=True)
							data_waterConsumption.drop([i + j], inplace=True)
							data_qr_np1.drop([i + j], inplace=True)
							data_qr_np2.drop([i + j], inplace=True)
							data_qr_np3.drop([i + j], inplace=True)
							data_qr_np4.drop([i + j], inplace=True)
							j += 1
							h,m,s = data_time_waterConsumption.iloc[i + j, 0].split(':')
					else:
						h_i = h

			# Reset indexes			
			data_time.reset_index(drop=True, inplace=True)
			data_date.reset_index(drop=True, inplace=True)
			data_waterConsumption.reset_index(drop=True, inplace=True)
			data_qr_np1.reset_index(drop=True, inplace=True)
			data_qr_np2.reset_index(drop=True, inplace=True)
			data_qr_np3.reset_index(drop=True, inplace=True)
			data_qr_np4.reset_index(drop=True, inplace=True)			

			# Timesteps
			data_length = len(data_qr_np3)

			QBP= np.zeros(data_length)
			HBP = np.zeros(data_length)
			etaBP = np.ones(data_length)

			Hanlage = []

			print('NP1: '+str(len(data_qr_np1))+'NP2: '+str(len(data_qr_np2))+'NP3: '+str(len(data_qr_np3))+'NP4: '+str(len(data_qr_np4)))

			for i in range(data_length):
				if(pd.isnull(data_waterConsumption.iloc[i, 0]) or pd.isna(data_waterConsumption.iloc[i, 0]) or data_waterConsumption.iloc[i, 0] <= 0):
					data_waterConsumption.iloc[i, 0] = last_valid_waterConsumption		
				else:
					last_valid_waterConsumption = data_waterConsumption.iloc[i, 0]
			
				# Count the number of days by month	
				# if(i == 0):
				# 	d,m,y = data_date.iloc[i, 0].split('.')
				# 	h_i,m_i,s_i = data_time_waterConsumption.iloc[i, 0].split(':')
				# else:
				# 	d_i,m,y = data_date.iloc[i, 0].split('.')
				# 	if(d_i != d):
				# 		days_counter += 1
				# 		d = d_i	


				# Count the number of days by month	
				if i == 0 and indexm == 0:
					#d,m,y = data_date.iloc[i, 0].split('.')
					h,m,s = data_time.iloc[i, 0].split(':')
					h_i = h
					#d_i = d	
				#else:
					#d_i,m,y = data_date.iloc[i, 0].split('.')

				# Reset the tank levels	
				if i == 0:
				# Reset the list of the tank levels
					HB_temp = HB[len(HB) - 1]
					HB = []
					HB.append(HB_temp)

				h,m,s = data_time.iloc[i, 0].split(':')
				data_time.iloc[i, 0] = int(datetime.timedelta(hours=int(h),minutes=int(m),seconds=int(s)).total_seconds())

				aa = p1 * data_waterConsumption.iloc[i, 0] + p2
				bb = p3 * data_waterConsumption.iloc[i, 0] + p4
				cc = HB[i]

				Qanlage_1 = list(map(lambda x:aa*pow(x,2),Qanlage))
				Qanlage_2 = list(map(lambda x:bb * x + cc,Qanlage))
				Hanlage.append(list(map(add, Qanlage_1, Qanlage_2)))

				if((pd.isnull(data_qr_np1.iloc[i, 0]) or pd.isna(data_qr_np1.iloc[i, 0])) and last_action == 1):
					action_index = 1
				elif((pd.isnull(data_qr_np2.iloc[i, 0]) or pd.isna(data_qr_np2.iloc[i, 0])) and last_action == 2):		
					action_index = 2
				elif((pd.isnull(data_qr_np3.iloc[i, 0]) or pd.isna(data_qr_np3.iloc[i, 0])) and last_action == 3):		
					action_index = 3
				elif((pd.isnull(data_qr_np4.iloc[i, 0]) or pd.isna(data_qr_np4.iloc[i, 0])) and last_action == 4):		
					action_index = 4
				elif(data_qr_np1.iloc[i, 0] != 0):
					action_index = 1
				elif(data_qr_np2.iloc[i, 0] != 0):
					action_index = 2
				elif(data_qr_np3.iloc[i, 0] != 0):
					action_index = 3
				elif(data_qr_np4.iloc[i, 0] != 0):
					action_index = 4			
				else: action_index = 0	

				if(action_index > 0):
					if(action_index == 1):
						HPumpe = H1
						ETAPumpe = pETA1
					elif(action_index == 2):
						HPumpe = H2
						ETAPumpe = pETA2
					elif(action_index == 3):
						HPumpe = H3
						ETAPumpe = pETA3
					elif(action_index == 4):
						HPumpe = H4
						ETAPumpe = pETA4

					res = np.argmin(abs(Hanlage[i]-HPumpe))	

					QBP[i] = Qanlage[res]
					HBP[i] = HPumpe[res]
					etaBP[i] = np.polyval(ETAPumpe, QBP[i])
				else: 
					QBP[i] = 0
					HBP[i] = 0
					etaBP[i] = 1

				# Tank level	
				#HB.append(HB[i]+(QBP[i]-data_waterConsumption.iloc[i, 0])/60/A)
				HB_i = 	HB[i]+(QBP[i]-data_waterConsumption.iloc[i, 0])/60/A
				if HB_i < 47:
					HB.append(47)
				elif HB_i > 57:
					HB.append(57)
				else:
					HB.append(HB_i)
				print('Tank level: '+str(HB[i+1]))
			
				###############################################################################################
				######################################### EVALUATIONS #########################################

				''' ELECTRICITY CONSUMPTION EVALUATION '''
				pbp = ((rho*g*QBP[i]/3600*HBP[i])/55*etaBP[i])/1000

				if(action_index == 4):
					consume_np4 += pbp
				elif(action_index == 3):
					consume_np3 += pbp
				elif(action_index == 2):
					consume_np2 += pbp
				elif(action_index == 1):
					consume_np1 += pbp
				total_consume += pbp

				''' PUMP SWITCHES EVALUATION '''
				# Count 1 to ON + 1 to OFF at once
				if action_index != last_action:
					if action_index == 1:
						switches_np1 += 2
					elif action_index == 2:
						switches_np2 += 2
					elif action_index == 3:
						switches_np3 += 2
					elif action_index == 4:
						switches_np4 += 2

				''' TANK LEVEL EVALUATION '''
				# Check if the hour have changed
				if(minutes_counter < 59):
					minutes_counter += 1
					tk1_level += HB[i]
				# Group the average consume by hour					
				elif(h_i != h or minutes_counter >= 59):
					average_tank_level[int(h), indexm] += tk1_level/minutes_counter
					tk1_level = 0
					minutes_counter = 0
					h_i = h

				# Update the last action performed
				last_action = action_index	

				if data_time.iloc[i, 0] == 86399:
					days_counter += 1	
				
			average_electricity_month_np1[indexm] = consume_np1/days_counter
			average_electricity_month_np2[indexm] = consume_np2/days_counter
			average_electricity_month_np3[indexm] = consume_np3/days_counter
			average_electricity_month_np4[indexm] = consume_np4/days_counter

			average_cycles_month_np1[indexm] = switches_np1/days_counter
			average_cycles_month_np2[indexm] = switches_np2/days_counter
			average_cycles_month_np3[indexm] = switches_np3/days_counter
			average_cycles_month_np4[indexm] = switches_np4/days_counter

			average_tank_level[:, indexm] /= days_counter
			# To correct missing data in Maerz
			if MONTHS[indexm] == 'Maerz':
				average_tank_level[2, 2] *= days_counter
				average_tank_level[2, 2] /= (days_counter - 1)

		y1 = average_electricity_month_np1
		y2 = average_electricity_month_np2
		y3 = average_electricity_month_np3
		y4 = average_electricity_month_np4

		f.write('NP1-KW: '+str(y1)+'\n')
		f.write('NP2-KW: '+str(y2)+'\n')
		f.write('NP3-KW: '+str(y3)+'\n')
		f.write('NP4-KW: '+str(y4)+'\n')
		f.write('TOTAL KW CONSUMPTION: '+str(total_consume)+'\n')	
	
		# Plot the points  
		plt.plot(MONTHSL, y1, label='NP1')
		plt.plot(MONTHSL, y2, label='NP2')
		plt.plot(MONTHSL, y3, label='NP3')
		plt.plot(MONTHSL, y4, label='NP4')

		# Name the y axis 
		plt.ylabel('Average Electricity Consumption (kW)',fontsize=14)

		# Incline the x label and change the font size
		plt.xticks(rotation=45, ha='right', fontsize=12)
		
		# Name the x axis 
		#plt.xlabel('Month') 
		  
		# Giving a title to my graph 
		plt.title('Average Electricity Consumption per Month in '+YEAR,fontsize=14)

		# Show a legend on the plot
		plt.legend()

		# Function to show the grid in the background
		plt.grid(alpha=0.2, linestyle='--')

		plt.tight_layout()
		  
		# Function to show the plot 
		#plt.show()

		# Saving the file
		plt.savefig('ElectricityConsumption'+YEAR+'.png') 

		# Clear the figure
		plt.clf()

		#Plot tank level
		x = ['00:00', '01:00', '02:00', '03:00', '04:00' , '05:00', '06:00', '07:00', '08:00' ,'09:00' ,'10:00', '11:00', '12:00',
			'13:00', '14:00', '15:00', '16:00', '17:00', '18:00', '19:00', '20:00', '21:00', '22:00', '23:00']

		for (j, month) in enumerate(MONTHS):	
			y = average_tank_level[:,j]	
	
			# Plotting the points  
			plt.plot(x, y, label=MONTHSL[j])
			plt.xticks(rotation=90, ha='right', fontsize=10) 

		# Naming the x axis 
		plt.xlabel('Time of Day', fontsize=14) 
		# Naming the y axis 
		plt.ylabel('Average Level (m)', fontsize=14) 

		plt.ylim([50.5, 57])
		  
		# Giving a title to the graph 
		plt.title('Tank Level per Month in '+YEAR, fontsize=14)

		# Show a legend on the plot
		plt.legend()

		# Function to show the grid in the background
		plt.grid(alpha=0.2, linestyle='--')

		plt.tight_layout()
		  
		# Function to show the plot 
		#plt.show()

		# Saving the file
		plt.savefig('TankLevel_evaluation'+YEAR+'.png') 

		# Clear the figure
		plt.clf()

		#Plot cycles in the pump operation
		y1 = average_cycles_month_np1
		y2 = average_cycles_month_np2
		y3 = average_cycles_month_np3
		y4 = average_cycles_month_np4

		f.write('NP1 - SWITCHES: '+str(y1)+'\n')
		f.write('NP2 - SWITCHES'+str(y2)+'\n')
		f.write('NP3 - SWITCHES'+str(y3)+'\n')
		f.write('NP4 - SWITCHES'+str(y4)+'\n')

		f.close()
	
		# Plotting the points  
		plt.plot(MONTHSL, y1, label='NP1')
		plt.plot(MONTHSL, y2, label='NP2')
		plt.plot(MONTHSL, y3, label='NP3')
		plt.plot(MONTHSL, y4, label='NP4')

		# Naming the y axis 
		plt.ylabel('Average Daily Switches ON/OFF', fontsize=14)

		# Naming the x axis 
		#plt.xlabel('Month')

		# Incline the x label
		plt.xticks(rotation=45, ha='right', fontsize=10)

		# Giving a title to the graph 
		plt.title('Average Daily Switches ON/OFF per Month in '+YEAR, fontsize=14)

		# Show a legend on the plot
		plt.legend()

		# Function to show the grid in the background
		plt.grid(alpha=0.2, linestyle='--')

		plt.tight_layout()
		  
		# Function to show the plot 
		#plt.show()

		# Saving the file
		plt.savefig('PumpSwitches_evaluation'+YEAR+'.png') 

		# Clear the figure
		plt.clf()	

	def __init__(self):

		self.evaluate()

if __name__ == '__main__':
	obj = Evaluation()							