'''

This code follows the example found in: https://scikit-learn.org/stable/modules/generated/sklearn.linear_model.LogisticRegression.html
& https://github.com/sfujim/BCQ/blob/4876f7e5afa9eb2981feec5daf67202514477518/discrete_BCQ/discrete_BCQ.py#L5

'''
import numpy as np
from sklearn.linear_model import LogisticRegression
from sklearn.model_selection import RepeatedStratifiedKFold
from sklearn.model_selection import GridSearchCV
import time

# Actions available
ACTIONS = [[0,0,0,0], [1,0,0,0], [0,1,0,0], [0,0,1,0], [0,0,0,1]]				

# Threshold	
TAU = 0.3

class GenerativeModel:

	def get_data(self):
		
		with open ("replay_memory.txt", "r") as file:
			for line in file:
				line.rstrip()
				if line.find('\x00') == -1:
					transition = eval(line)
					state_feat = transition[0] 
					self.states.append(state_feat)
					self.actions.append(ACTIONS.index(transition[1]))

		self.clf = LogisticRegression(penalty='l1', C=10, solver='liblinear').fit(self.states, self.actions)				

	def get_qmax(self, current_state, q_values):
		p = self.clf.predict_proba([current_state])
		p = np.exp(p)
		#p = (p/np.amax(p, 0,  keepdims=True)[0] > TAU)
		p = (p/np.amax(p) > TAU)

		#max_future_q = np.amax((p * q + (1. - p) * -1e8), 0)
		max_future_q = np.amax(p * q_values + (1. - p) * -1e8)
		
		return max_future_q

	def get_action(self, current_state):
		p = self.clf.predict_proba([current_state])
		max_future_q = np.argmax(p)
		return max_future_q

	def grid_search(self):
	
		model = LogisticRegression()
		cv = RepeatedStratifiedKFold(n_splits=10, n_repeats=3, random_state=1)
		space = dict()
		space['solver'] = ['newton-cg', 'lbfgs', 'liblinear']
		space['penalty'] = ['none', 'l1', 'l2', 'elasticnet']
		space['C'] = [1e-5, 1e-4, 1e-3, 1e-2, 1e-1, 1, 10, 100]

		# define search
		search = GridSearchCV(model, space, scoring='accuracy', n_jobs=-1, cv=cv)

		# execute search
		result = search.fit(self.states, self.actions)
		# summarize result
		print('Best Score: %s' % result.best_score_)
		print('Best Hyperparameters: %s' % result.best_params_)	

	def __init__(self):
		self.states = []
		self.actions = []

		self.get_data()
		#self.grid_search()	

if __name__ == '__main__':
	obj = GenerativeModel()		
