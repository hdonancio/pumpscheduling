"""

This class is adapted & modified from the code found on
https://github.com/pythonlessons/Reinforcement_Learning/tree/master/05_CartPole-reinforcement-learning_PER_D3QN

Code related to the paper: Prioritized Experience Replay

Source: https://gitlab.com/hdonancio/pumpscheduling

""" 
import numpy as np

# How much priorization is used (0 - no prioritization, 1 - full prioritization)
ALPHA = 0.6
BETA_INCREMENT = 0.0001
#To avoid sample not be replayed
EPSILON = 0.1

class PER:

	def add(self, transition, priority):
		sumtree_idx = self.write + self.capacity - 1

		# Store the transition in the tree
		self.data[self.write] = transition

		# Update the priorities
		self.update(sumtree_idx, priority)

		# Update counter
		self.write += 1

		# If achieve the capacity, start override
		if self.write >= self.capacity:
			self.write = 0

		# Update the number of entries	
		if self.n_entries < self.capacity:
			self.n_entries += 1	

	''' Update the priorities of the tree '''		
	def update(self, sumtree_idx, priority):
		# Change the priority
		change = priority - self.sumtree[sumtree_idx]
		self.sumtree[sumtree_idx] = priority

		# Update the priorities
		while sumtree_idx != 0:
			sumtree_idx = (sumtree_idx - 1) // 2
			self.sumtree[sumtree_idx] += change

	def get_leaf(self, v):
		parent_idx = 0

		while True:
			left_child_idx = 2 * parent_idx + 1
			right_child_idx = left_child_idx + 1

			# If we reach bottom, end the search
			if left_child_idx >= len(self.sumtree):
				leaf_idx = parent_idx
				break
			else: # downward search, always search for a higher priority node
				if v <= self.sumtree[left_child_idx]:
					parent_idx = left_child_idx
				else:
					v -= self.sumtree[left_child_idx]
					parent_idx = right_child_idx

		transition_idx = leaf_idx - self.capacity + 1

		return leaf_idx, self.sumtree[leaf_idx], self.data[transition_idx]

	def sample(self, n):
		minibatch = []

		b_idx = np.empty((n,), dtype=np.int32)
		#ISWeights = np.empty((n, 1))
		ISWeights = np.empty(n)

		priority_segment = self.sumtree[0] / n
		self.beta = np.min([1., self.beta + BETA_INCREMENT])

		# min_prob = np.min(self.sumtree[-self.capacity:]) / self.sumtree[0]
		# if min_prob == 0:
		# 	min_prob = 0.00001  

		for i in range(n):
			# A value is uniformly sample from each range
			a, b = priority_segment * i, priority_segment * (i + 1)
			value = np.random.uniform(a, b)

			# Experience that correspond to each value is retrieved
			idx, priority, transition = self.get_leaf(value)

			b_idx[i]= idx

			prob = priority / self.sumtree[0]
			
			#ISWeights[i, 0] = np.power(self.n_entries * prob, -self.beta)

			ISWeights[i] = np.power(self.n_entries * prob, -self.beta)

			minibatch.append(transition)

		# Normalize the IS weights
		ISWeights /= ISWeights.max()	

		return b_idx, minibatch, ISWeights

	def batch_update(self, sumtree_idx, abs_errors):
		abs_errors += EPSILON
		 
		clipped_errors = np.minimum(abs_errors, self.absolute_error_upper)
		ps = np.power(clipped_errors, ALPHA)

		for ti, p in zip(sumtree_idx, ps):
			self.update(ti, p)	 

	def get_max(self):
		return np.max(self.sumtree[-self.capacity:])

	def get_beta(self):
		return self.beta	 		 			
	 
	def __init__(self, capacity, absolute_error_upper):
		self.write = 0
		self.n_entries = 0
		self.beta = 0.4
		self.capacity = capacity
		self.absolute_error_upper = absolute_error_upper
		self.sumtree = np.zeros(2 * self.capacity - 1)
		self.data = np.zeros(capacity, dtype=object)
